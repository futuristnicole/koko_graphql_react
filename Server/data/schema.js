import { resolvers } from './resolvers';
import { makeExecutableSchema } from 'graphql-tools';

const typeDefs =`
    type Member {
      id: ID
      firstName: String!
      lastName: String!
      groups: [Group]
      playing: String
      description: String
      picLocaton: String

    }

    type Group {
      groups: Role
    }
    enum Role {
      STARRING
      SUPPORTING
      EXTRAS
    }

    type Query {
        getOneMember(id: ID): Member
        getMembers(id: ID): Member
    }

    input MemberInput {
      id: ID
      firstName: String!
      lastName: String!
      groups: [Group]
      playing: String
    }
    input GroupInput {
      groups: Role
    }

    type Mutation {
      createMember(input: MemberInput): Member
      updateMember(input: MemberInput): Member
      deleteMember(id: ID!): String
    }
`;

const schema = makeExecutableSchema({ typeDefs, resolvers});

export { schema };
