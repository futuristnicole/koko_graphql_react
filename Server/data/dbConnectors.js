import mongoose from 'mongoose';
import Sequelize from 'sequelize';
import _ from 'lodash';
import casual from 'casual';


// Mongo connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/members', {
});

const memberSchema = new mongoose.Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    groups: {
        type: String
    },
    playing: {
        type: String
    },
    description: {
        type: String
    },
    picLocaton: {
        type: String
    }
});

const Members = mongoose.model('members', memberSchema);

// // SQL 
// const sequelize = new Sequelize('database', null, null, {
//     dialect: 'sqlite',
//     storage: './pictures.squite'
// })

// const Pictures = sequelize.define('pictures', {
//     alt: { type: sequelize.String },
//     picLocaton: { type: sequelize.String },
//     gridType: { type: sequelize.String },
// })

// Pictures.sync({ force: true}).then(() => {
//     _.times(10, (i) => {
//         Pictures.create({
//             alt: casual._first_name,
//             picLocaton: casual._last_name,
//             gridType: casual.word,
//         })
//     })
// })

// const pictureSchema = new mongoose.Schema({
//     alt: {
//         type: String
//     },
//     picLocaton: {
//         type: String
//     },
//     gridType: {
//         type: Array
//     }
// });

// const Pictures = mongoose.model('pictures', pictureSchema);

export { Members };
