import mongoose from 'mongoose';
import { Members } from './dbConnectors';

// resolver map
export const resolvers = {
    Query: {
        getOneMember: (root, { id }) => {
            return new Promise((resolve, object) => {
                Members.findById(id, (err, members) => {
                    if (err) reject(err)
                    else resolve(member)
                });
            });
        },
        // getMembers: () => {
        //     return Members.findAll();
        // }
    },
    Mutation: {
        createMember: (root, { input }) => {
            const newFriend = new Members({
                firstName: input.firstName,
                lastName: input.lastName,
                groups: input.groups,
                playing: input.playing,
            });

            newMember.id = newMember._id;

            return new Promise((resolve, object) => {
                newMember.save((err) => {
                    if (err) reject(err)
                    else resolve(newMember)
                })
            })
        },
        updateMember: (root, { input }) => {
            return new Promise(( resolve, object) => {
                Members.findOneAndUpdate({ _id: input.id }, input, { new: true}, (err, member) => {
                    if (err) reject(err)
                    else resolve(member)
                })
            })
        },
        deleteMember: (root, { id }) => {
            return new Promise((resolve, object) => {
                Members.remove({ _id: id}, (err) => {
                    if (err) reject(err)
                    else resolve('Successfully deleted member')
                })
            })
        }
    },
};
